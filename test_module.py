import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

#first_name = "Janie"
#last_name = "Does"
first_name = "Vladyslava"
last_name = "Shvets"
phone_number = "000000000"
password = "password19"
gender = "male"
valid_country_code = "Poland"
valid_nationality = "BL"


class WizzairRegistration(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://wizzair.com/en-gb#/")
        self.driver.maximize_window()

    def tearDown(self):
        self.driver.quit()

    def test_registration(self):
        # 1 Click button Sign In on start page
        button_signin = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//button[@data-test = 'navigation-menu-signin']")))
        button_signin.click()

        """# 2 Click button Registration
        list_of_elements = self.driver.find_elements_by_xpath("//button[@class ='content__link1']")
        button_reg = list(filter(lambda x: "REGISTRATION" in x.text, list_of_elements))[0]
        button_reg.click()

        # 3 Type First and Last name
        type_name = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//input[@name = 'firstName']")))
        type_name.send_keys(first_name)
        type_name.send_keys(Keys.TAB + last_name)

        # 4 Choose gender
        if gender == 'male':
            male = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//label[@for = 'register-gender-male']")))
            self.driver.execute_script("arguments[0].click()", male)
        else:
            female = WebDriverWait(self.driver, 10).until( EC.element_to_be_clickable((By.XPATH, "//label[@for = 'register-gender-female']")))
            self.driver.execute_script("arguments[0].click()", female)

        # 5 Choose country code
        choose_country_code = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.phone-number__calling-code-selector__fake-input")))
        choose_country_code.click()
        all_country_codes = self.driver.find_elements_by_css_selector("li.phone-number__calling-code-selector__dropdown__item")
        country_code = list(filter(lambda x: valid_country_code in x.text, all_country_codes))[0]
        country_code.click()"""



        # Login on page for testing results
        
        type_email = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[@data-test = 'login-modal-email']")))
        type_email.send_keys('karmannayatigra@gmail.com')

        type_password = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[@data-test = 'login-modal-password']")))
        type_password.send_keys('p4ss2a1r')

        sigin = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//button[@data-test = 'loginmodal-signin']")))
        sigin.click()
        time.sleep(30)

        result = self.driver.find_element_by_xpath("//div[@class='rf-mobile-login']/*[1]")
        login_name = result.get_attribute('textContent').strip()
        user_name = (first_name + ' ' + last_name)
        self.assertTrue(login_name == user_name)


        """# 10 Choose nationality
        nationality = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.rf-input__inner>label>input[name = 'country-select']")))
        nationality.click()
        countries = self.driver.find_elements_by_css_selector("div.register-form__country-container__locations>label")
        #nationalities = self.driver.find_elements_by_xpath("//small[@class = 'register-form__country-container__location__abbr']")
        #nationalities = self.driver.find_elements_by_xpath("//strong[@class = 'register-form__country-container__location__name']")
        for country in countries:
            abbr_country = country.find_element_by_tag_name('small')
            if abbr_country.get_attribute('textContent') == valid_nationality:
                abbr_country.location_once_scrolled_into_view
                abbr_country.click()
                break
"""


        time.sleep(5)
        self.driver.save_screenshot("screenshot3.png")


if __name__ == '__main__':
    unittest.main(verbosity=2)