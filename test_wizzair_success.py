import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

first_name = "Janie"
last_name = "Roe"
phone_number = "000000000"
password = "password19"
gender = "female"
valid_country_code = "Poland"
valid_nationality = "BL"


class WizzairRegistration(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://wizzair.com/en-gb#/")
        self.driver.maximize_window()

    def tearDown(self):
        self.driver.quit()

    def test_registration(self):
        # 1 Click button Sign In on start page
        button_signin = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//button[@data-test = 'navigation-menu-signin']")))
        button_signin.click()

        # 2 Click button Registration
        list_of_elements = self.driver.find_elements_by_xpath("//button[@class ='content__link1']")
        button_reg = list(filter(lambda x: "REGISTRATION" in x.text, list_of_elements))[0]
        button_reg.click()

        # 3 Type first and last name
        type_name = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//input[@name = 'firstName']")))
        type_name.send_keys(first_name)
        type_name.send_keys(Keys.TAB + last_name)

        # 4 Choose gender
        gender_selector = "//label[@for = 'register-gender-male']" if gender == 'male' else "//label[@for = 'register-gender-female']"
        gender_choice = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, gender_selector)))
        self.driver.execute_script("arguments[0].click()", gender_choice)

        # 5 Choose country code
        choose_country_code = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.phone-number__calling-code-selector__fake-input")))
        choose_country_code.click()
        all_country_codes = self.driver.find_elements_by_css_selector("li.phone-number__calling-code-selector__dropdown__item")
        country_code = list(filter(lambda x: valid_country_code in x.text, all_country_codes))[0]
        country_code.click()

        # 6 Type phone number
        type_phone_number = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//input[@type='tel']")))
        type_phone_number.send_keys(phone_number)

        # 7 Go to page http://www.fakemailgenerator.com
        self.driver.execute_script("window.open('http://www.fakemailgenerator.com/')")
        WebDriverWait(self.driver, 10).until(EC.number_of_windows_to_be(2))
        mail_windows = self.driver.window_handles
        self.driver.switch_to.window(mail_windows[1])
        time.sleep(5)

        # 8 Copy the mail address
        copy_button = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.ID, "copy-button")))
        copy_button.click()

        # 9 Go to registration page
        self.driver.switch_to.window(mail_windows[0])

        # 10 Paste email
        type_email = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//input[@data-test = 'booking-register-email']")))
        type_email.send_keys(Keys.CONTROL, 'v')

        # 11 Type password
        type_password = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//input[@data-test = 'booking-register-password']")))
        type_password.send_keys(password)

        # 12 Choose nationality
        nationality = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.rf-input__inner>label>input[name = 'country-select']")))
        nationality.click()
        countries = self.driver.find_elements_by_css_selector("div.register-form__country-container__locations>label")

        for country in countries:
            abbr_country = country.find_element_by_tag_name('small')
            if abbr_country.get_attribute('textContent') == valid_nationality:
                abbr_country.location_once_scrolled_into_view
                abbr_country.click()
                break

        # 13 Click checkbox subscribe
        subscribe = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.gutter-bottom>span>label:nth-of-type(1)")))
        subscribe.click()

        # 14 Click checkbox accept
        accept = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.gutter-bottom.rf-checkbox__error-wrap>span>label:nth-of-type(1)")))
        accept.click()

        # 15 Click button Register
        register = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//button[@data-test ='booking-register-submit']")))
        self.driver.execute_script("arguments[0].click()", register)
        time.sleep(30)

        # 16 Check the result
        result = self.driver.find_element_by_xpath("//div[@class='rf-mobile-login']/*[1]")
        login_name = result.get_attribute('textContent').strip()
        user_name = (first_name + ' ' + last_name)
        self.assertTrue(login_name == user_name)

        self.driver.save_screenshot("screenshot.png")


if __name__ == '__main__':
    unittest.main(verbosity=2)
